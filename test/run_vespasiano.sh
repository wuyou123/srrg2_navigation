#!/bin/bash

xterm -e "roslaunch thin_navigation first_floor.launch" &
sleep 3
xterm -e "python nodino_del_signore.py" &
sleep 1
xterm -e "roslaunch thin_navigation first_floor_vespasiano.launch" &
sleep 3
xterm -e "rosrun rviz rviz -d robot_0.rviz" &

